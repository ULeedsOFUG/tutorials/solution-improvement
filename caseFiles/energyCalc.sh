#_________________ script to calculate volume-averaged kinetic energy from separate time directories

rm -rf tke      #  remove old data  
mkdir tke

#_________________ list all time directories 
x="foamListTimes"
y=`eval $x`
y="0 $y"               # include zero directory 
echo $y >> tke/timeDirsHorz.dat
tr -s ' '  '\n'< tke/timeDirsHorz.dat > tke/timeDirs.dat

#_________________ use OF utility to evaluate u_i*u_i
postProcess -func "magSqr(U)"

#_________________ now loop through time directories
for d in $y; do
echo $d
cd $d

#________ make a copy, with a friendlier name
cp "magSqr(U)" tempTKE

#_________________ remove unwanted text from file
sed -i -e 1,17d tempTKE                      # delete header, first 17 lines
n1=$(awk '/\(/{print NR}' tempTKE)           # line of first bracket '('
sed -i -e 1,${n1}d tempTKE                   # delete everything up to n1
n2=$(awk '/\)/{print NR}' tempTKE)           # line of last bracket ')'
n3=$(cat tempTKE | wc -l)                    # number of lines in file
sed -i -e ${n2},${n3}d tempTKE               # delete between n2 and n3

#_________________ calculate averages for each time directory

# read file, calculate mean to 10 dp (%.10f), 
cat tempTKE \
| perl -M'List::Util qw(sum)' -MPOSIX -0777 -a -ne 'printf "%.10f"x1 ,  0.5*sum(@F)/@F;' >> ../tke/tkeAll.dat

sed -i -e '$a\' ../tke/tkeAll.dat     # add a space at end of file
rm temp*          # clear temp files
cd ..
done       # end of loop

#_________________ normalise tke data for plotting
maxVal=$(sort -t= -nr -k3 tke/tkeAll.dat | head -1)                       # find max value
awk -v y="$maxVal" '{print $1/y}' tke/tkeAll.dat > tke/tkeAllNorm.dat     # normalise by max value
paste tke/timeDirs.dat tke/tkeAllNorm.dat > tke/tkeTime.dat               # bring together time and energy columns
