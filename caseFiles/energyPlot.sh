#!/bin/bash
# bash script to plot energy decay over time

# labels
set title "Energy dissipation" font ",18" 
set xlabel "Time (s)" font ",16"
set ylabel "Ek/max(Ek)" font ",16"

# set axis range and grid lines
set xr [0:5]
set grid ytics mytics xtics mxtics  # grid lines
set mytics 2                        # set tick spacing
set grid

# plot data
plot "tke/tkeTime.dat" with lines lw 2,\
"deBonis2013.dat" with lines lw 2 

# terminal control
pause -1 "Hit any key to continue"
